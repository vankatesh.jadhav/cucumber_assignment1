Feature: Validate demo web Shop 
Scenario Outline: validate the demo web shop login functionality.
Given Open the website
Then the home page of website get displayed
When user click on login1 button
And user enter the email "<USERNAME>" in email textbox
And user enter the password "<PASSWORD>" in password textbox
Then user click on login button
And the home page  with logged user name should display
Then user click on logout button
Examples:
      | USERNAME                   |  PASSWORD    |
      | demo2023@demo.com          |  Demo@123  |
      | demo2023@demo.com          |  Demo@123    |
