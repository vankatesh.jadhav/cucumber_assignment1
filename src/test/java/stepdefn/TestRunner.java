package stepdefn;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features="src/test/resources/features",glue={"stepdefn"})
public class TestRunner extends AbstractTestNGCucumberTests 
{

}
